'use strict';

angular.module('proiectApp')
  .controller('MyProfileSavedCtrl', function ($scope, $timeout, $location, $http) {
    $scope.goToMyProfile = function () {
      $timeout(function () {
        $location.path("/myProfile");
      }, 1);
    };

    //GET request for Saved Profile
    $http.get('http://5.2.191.107:11111/api/user/search/' + sessionStorage.getItem('id'), {
        // params:  {page: 1, limit: 100, sort: 'name', direction: 'desc'},
        headers: {'Authorization': 'Basic ' + btoa(unescape(encodeURIComponent('email@gmail.com' + ':' + 'parola')))}
      }
    )
      .then(function (response) {
        console.dir(response);
        $scope.profile =
          {
            name: response.data['name'],
            gender: response.data['gender'],
            user_type: response.data['user_type'],
            role: response.data['role'],
            type: response.data['type'],
            address: response.data['address'],
            phone_number: response.data['phone_number'],
            email: response.data['email'],
            country: response.data['country']
          };
        $scope.refreshSavedPage = function ()
        {
          $location.reload();
        }
      }, function (x) {
        console.dir(x);
      });
  });
