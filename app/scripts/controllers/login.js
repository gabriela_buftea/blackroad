'use strict';

angular.module('proiectApp')
  .service('Session', function ($scope) {
    $scope.create = function (data) {
      $scope.email = data.email;
      $scope.password = data.password;
      $scope.id = data.id;
      $scope.loggedIn = true;
    };
    $scope.destroy = function () {
      $scope.email = null;
      $scope.password = null;
      $scope.id = null;
    }
  })
  .controller('LoginCtrl', function ($scope, $location, $anchorScroll, $http, $timeout) {
    $scope.temp = {
      email: '',
      password: ''
    };

    if(sessionStorage.getItem("logged") == 'true')
      $location.path('/myProfileSaved');

    $scope.goToMyProfileSaved = function () {
      var string = btoa($scope.temp.email);
      window.localStorage.setItem('LogIn2', string);
      $timeout(function () {
        $location.path("/myProfileSaved");
      }, 1000);
    };

    $scope.goToSignin = function () {
      $timeout(function () {
        $location.path("/signin");
      }, 1);
    };

    $scope.hideSignIn = true;
    $timeout(function () {
      $scope.hideSignIn = true;
    }, 1);

    $scope.getLocalProfile = function (callback) {
      var profileImgSrc = localStorage.getItem("PROFILE_IMG_SRC");
      var profileName = localStorage.getItem("PROFILE_NAME");
      var profileReAuthEmail = localStorage.getItem("PROFILE_REAUTH_EMAIL");

      if (profileName !== null
        && profileReAuthEmail !== null
        && profileImgSrc !== null) {
        callback(profileImgSrc, profileName, profileReAuthEmail);
      }
    };

    $scope.loadProfile = function () {
      if (!$scope.supportsHTML5Storage()) {
        return false;
      }
      $scope.getLocalProfile(function (profileImgSrc, profileName, profileReAuthEmail) {
        $("#profile-img").attr("src", profileImgSrc);
        $("#profile-name").html(profileName);
        $("#reauth-email").html(profileReAuthEmail);
        $("#inputEmail").hide();
        $("#remember").hide();
      });
    };

    $scope.supportsHTML5Storage = function () {
      try {
        return 'localStorage' in window && window['localStorage'] !== null;
      } catch (e) {
        return false;
      }
    };

    $scope.testLocalStorageData = function () {
      if (!supportsHTML5Storage()) {
        return false;
      }
      localStorage.setItem("PROFILE_IMG_SRC", "//lh3.googleusercontent.com/-6V8xOA6M7BA/AAAAAAAAAAI/AAAAAAAAAAA/rzlHcD0KYwo/photo.jpg?sz=120");
      localStorage.setItem("PROFILE_NAME", "César Izquierdo Tello");
      localStorage.setItem("PROFILE_REAUTH_EMAIL", "oneaccount@gmail.com");
    };

    $(document).ready(function () {
      $scope.loadProfile();
    });

    //POST request for LOGIN
    $scope.loginRequest = function () {
      var data = {
        email: $scope.temp.email,
        password: $scope.temp.password
      };
      var $http = new XMLHttpRequest();
      $http.open('POST', 'http://5.2.191.107:11111/api/login', true);

      //Send the proper header information along with the request
      $http.setRequestHeader('Content-type', 'application/json');
      $http.setRequestHeader('Authorization', 'Basic ' + btoa(unescape(encodeURIComponent('email@gmail.com' + ':' + 'parola'))));

      $http.onreadystatechange = function () {//Call a function when the state changes.
        if ($http.readyState == 4 && $http.status == 200) {
          var json = JSON.parse($http.responseText);
          console.dir(json);
          if (json['status'] == 'success') {
            var data = {
                email: $scope.temp.email,
                password: $scope.temp.password,
                id: json['id_user']
            };
            sessionStorage.setItem("email", data.email);
            sessionStorage.setItem("password", data.password);
            sessionStorage.setItem("id", data.id);
            sessionStorage.setItem("logged", 'true');
          }
          else if (json['status'] == 'failed') {
            alert("This email does not correspond with the password!");
            window.location.reload(true);
          }
        }
      };
      $http.send(JSON.stringify(data));
    }
  });
