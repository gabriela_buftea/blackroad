'use strict';

/**
 * @ngdoc function
 * @name proiectApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the proiectApp
 */
angular.module('proiectApp')
  .controller('MainCtrl', function ($scope, $location, $anchorScroll, $http) {
    $scope.temp = {
      from: {
        name: '',
        lat: 'pace.geometry.location.lat()',
        lng: 'pace.geometry.location.lng()'
      },
      to: {
        name: '',
        lat: 'pace.geometry.location.lat()',
        lng: 'pace.geometry.location.lng()'
      }
    };
    $scope.nrPasageri = '1';
    $scope.listaMasini = [];
    $scope.maxPassengerList = [];
    $scope.masinaSelectata = {};


    var map = new google.maps.Map(document.getElementById('googleMap'),
      {
        center: {lat: 44.429892, lng: 26.062526},
        zoom: 13
      });


    for (var i = 1; i < 6; i++) {
      $scope.maxPassengerList.push(i);
    }

    $http.get('http://5.2.191.107:11111/api/cartype/capacity/' + $scope.nrPasageri, {
        // params:  {page: 1, limit: 100, sort: 'name', direction: 'desc'},
        headers: {'Authorization': 'Basic ' + btoa(unescape(encodeURIComponent('email@gmail.com' + ':' + 'parola')))}
      }
    )
      .then(function (response) {
        $scope.listaMasini = [];
        $scope.listaMasini = response.data;
        var lisaSalveazaMasini = angular.copy($scope.listaMasini);
        angular.forEach(lisaSalveazaMasini, function (masina, key) {
          if (masina.capacity >= $scope.nrPasageri) {
            $scope.listaMasini.push(masina);
          }
        });
      });

    $scope.element = document.getElementById('nr_persons');
    $scope.value = $scope.element.value;
    if ($scope.value == '') {
      $scope.value = 1;
      console.dir("da");
    } else {
      console.dir("nu");
    }

    $scope.filtreazaMasini = function (item) {
      console.dir($scope.masinaSelectata);
      $scope.listaMasini = [];
      $http.get('http://5.2.191.107:11111/api/cartype/capacity/' + item, {
          // params:  {page: 1, limit: 100, sort: 'name', direction: 'desc'},
          headers: {'Authorization': 'Basic ' + btoa(unescape(encodeURIComponent('email@gmail.com' + ':' + 'parola')))}
        }
      )
        .then(function (response) {
          $scope.listaMasini = response.data;
          var lisaSalveazaMasini = angular.copy($scope.listaMasini);
          angular.forEach(lisaSalveazaMasini, function (masina, key) {
            if (masina.capacity >= $scope.nrPasageri) {
              $scope.listaMasini.push(masina);
            }
          });
          console.dir($scope.listaMasini);
        });
    };

    //GET request for Voucher
    $http.get('http://5.2.191.107:11111/api/voucher', {
        // params:  {page: 1, limit: 100, sort: 'name', direction: 'desc'},
        headers: {'Authorization': 'Basic ' + btoa(unescape(encodeURIComponent('email@gmail.com' + ':' + 'parola')))}
      }
    )
      .then(function (response) {
        console.dir(response);
      }, function (x) {
        console.dir(x);
      });

    //POST request for calculate and display price
    $scope.priceRequest = function () {
      var json = JSON.parse(document.getElementById('car_type').value);
      var data = {
        id_user: sessionStorage.getItem("id"),
        from_address: document.getElementById('from').value,
        to_address: document.getElementById('to').value,
        from_lat: $scope.temp.from.lat,
        from_lng: $scope.temp.from.lng,
        to_lat: $scope.temp.to.lat,
        to_lng: $scope.temp.to.lng,
        cartype: json['type'],
        voucher: document.getElementById('voucher').value,
        id_company: "1",
        email: document.getElementById('email').value,
        phone_number: document.getElementById('phone_number').value
      };
      var $http = new XMLHttpRequest();
      $http.open('POST', 'http://5.2.191.107:11111/api/form', true);

      //Send the proper header information along with the request
      $http.setRequestHeader('Content-type', 'application/json');
      $http.setRequestHeader('Authorization', 'Basic ' + btoa(unescape(encodeURIComponent('email@gmail.com' + ':' + 'parola'))));

      $http.onreadystatechange = function () {//Call a function when the state changes.
        if ($http.readyState == 4 && $http.status == 200) {
          var json = JSON.parse($http.responseText);
          console.dir(json);
          $scope.price = json[0]['price'];
          $scope.duration = json[0]['duration'];
        }
      };
      $http.send(JSON.stringify(data));
    };

    //POST request for booking
    $scope.bookingRequest = function () {
      var json = JSON.parse($scope.masinaSelectata);
      var data = {
        id_company: '1',
        id_user: Number(sessionStorage.getItem("id")),
        cartype: json.type,
        from_address: document.getElementById('from').value,
        to_address: document.getElementById('to').value,
        from_lat: '' + $scope.temp.from.lat,
        from_lng: '' + $scope.temp.from.lng,
        to_lat: '' + $scope.temp.to.lat,
        to_lng: '' + $scope.temp.to.lat,
        distance: document.getElementById('distance').textContent.trim(),
        duration: document.getElementById('element8').textContent.trim(),
        price: document.getElementById('element3').textContent.trim(),
        payment_method: document.getElementById('payment').value,
        name: document.getElementById('name').value,
        email: $scope.email,
        phone_number: document.getElementById('phone_number').value,
        time: document.getElementById('time').value,
        date: document.getElementById('date').value
      };
      console.dir(data);
      var $http = new XMLHttpRequest();
      $http.open('POST', 'http://5.2.191.107:11111/api/booking', true);

      //Send the proper header information along with the request
      $http.setRequestHeader('Content-type', 'application/json');
      $http.setRequestHeader('Authorization', 'Basic ' + btoa(unescape(encodeURIComponent('email@gmail.com' + ':' + 'parola'))));

      $http.onreadystatechange = function () {//Call a function when the state changes.
        if ($http.readyState == 4 && $http.status == 200) {
          console.dir($http.responseText);
        }
      };
      $http.send(JSON.stringify(data));
    };

    //GET request for showing drivers and them status
    $http.get('http://5.2.191.107:11111/api/driverstatus/logged', {
        // params:  {page: 1, limit: 100, sort: 'name', direction: 'desc'},
        headers: {'Authorization': 'Basic ' + btoa(unescape(encodeURIComponent('email@gmail.com' + ':' + 'parola')))}
      }
    )
      .then(function (response) {

        $scope.features = [];
        for (i = 0; i < response.data.length; i++) {
          var json = {
            position: new google.maps.LatLng(response.data[i]['lat'], response.data[i]['lng']),
            id: response.data[i]['id'],
            status: response.data[i]['status'],
            job: response.data[i]['job']
          }
          $scope.features.push(json);
        }
        console.dir($scope.features);
        var iconBase = 'http://maps.google.com/mapfiles/kml/paddle/';
        var icons = {
          dow: {
            icon: iconBase + 'blu-blank.png'
          },
          dap: {
            icon: iconBase + 'purple-blank.png'
          },
          pob: {
            icon: iconBase + 'P.png'
          }
        };

        $scope.features.forEach(function (feature) {
          var marker = new google.maps.Marker({
            position: feature.position,
            icon: icons[feature.job].icon,
            map: map
          });
          var infowindow = new google.maps.InfoWindow({
            content: feature.status
          });
          google.maps.event.addListener(marker, 'mouseover', function () {
            infowindow.open(map, marker);
          });
          google.maps.event.addListener(marker, 'mouseout', function () {
            infowindow.close(map, marker);
          });
        });
      }, function (x) {
        console.dir(x);
      });

    function isNumeric(n) {
      return !isNaN(parseFloat(n)) && isFinite(n);
    }

    $scope.changeAddress = function (type, value) {

      if (type == 'from') {
        $scope.temp.from.name = document.getElementById('from').value;
        if (value && value.geometry && value.geometry.location) {
          $scope.temp.from.lat = value.geometry.location.lat();
          $scope.temp.from.lng = value.geometry.location.lng();
        }
      }
      else {
        $scope.temp.to.name = document.getElementById('to').value;
        if (value && value.geometry && value.geometry.location) {
          $scope.temp.to.lat = value.geometry.location.lat();
          $scope.temp.to.lng = value.geometry.location.lng();
        }
      }

      // Call calculeazaDistanta function only if you receive numeric coordinates
      if (isNumeric($scope.temp.from.lat) && isNumeric($scope.temp.from.lng) && isNumeric($scope.temp.to.lat) && isNumeric($scope.temp.to.lng)) {
        $scope.calculeazaDistanta();
        // $scope.calculatePriceAccordingToKm();
      }
    };

    // Place pin for fields From and To and display route between them
    var init = function () {
      var directionsService = new google.maps.DirectionsService;
      var directionsDisplay = new google.maps.DirectionsRenderer;

      directionsDisplay.setMap(map);

      var onChangeHandler = function () {
        $scope.calculateAndDisplayRoute(directionsService, directionsDisplay);
      };

      document.getElementById('from').addEventListener('change', onChangeHandler);
      document.getElementById('to').addEventListener('change', onChangeHandler);

      // Place marker A from
      var markerFrom = new google.maps.Marker({
        map: map,
        anchorPoint: new google.maps.Point('from')
      });

      // Place marker B to
      var markerTo = new google.maps.Marker({
        map: map,
        anchorPoint: new google.maps.Point('to')
      });

      //From locatie
      var inputFrom = document.getElementById('from');
      // map.controls[google.maps.ControlPosition.TOP_RIGHT].push(card);
      var autocomplete = new google.maps.places.Autocomplete(inputFrom);
      autocomplete.setFields(
        ['address_components', 'geometry', 'icon', 'name']);

      autocomplete.addListener('place_changed', function () {

        var place = autocomplete.getPlace();
        if (!place.geometry) {
          // User entered the name of a Place that was not suggested and
          // pressed the Enter key, or the Place Details request failed.
          window.alert("No details available for input: '" + place.name + "'");
          return;
        }
        markerFrom.setPosition(place.geometry.location);
        markerFrom.setVisible(true);
        map.setCenter(place.geometry.location);
        map.setZoom(17);  // Why 17? Because it looks good.
        $scope.changeAddress('from', place);
      });

      //To destinatie
      var inputTo = document.getElementById('to');
      // map.controls[google.maps.ControlPosition.TOP_RIGHT].push(card);
      var autocomplete2 = new google.maps.places.Autocomplete(inputTo);
      autocomplete2.setFields(
        ['address_components', 'geometry', 'icon', 'name']);

      autocomplete2.addListener('place_changed', function () {

        var place = autocomplete2.getPlace();
        if (!place.geometry) {
          // User entered the name of a Place that was not suggested and
          // pressed the Enter key, or the Place Details request failed.
          window.alert("No details available for input: '" + place.name + "'");
          return;
        }
        if (markerTo) {
          markerTo.setPosition(place.geometry.location);
          markerTo.setVisible(true);
        }

        map.setCenter(place.geometry.location);
        map.setZoom(17);  // Why 17? Because it looks good.

        $scope.changeAddress('to', place);
      });
    };

    // Calculate rout on map From => To
    $scope.calculateAndDisplayRoute = function (directionsService, directionsDisplay) {
      directionsService.route({
        origin: document.getElementById('from').value,
        destination: document.getElementById('to').value,
        travelMode: 'DRIVING'
      }, function (response, status) {
        if (status === 'OK') {
          directionsDisplay.setDirections(response);
        } else {
          // window.alert('Directions request failed due to ' + status);
        }
      });
    };

    setTimeout(function () {
      init();
    }, 1000);

    $scope.scrollTo = function (where) {
      $location.hash(where);
      $anchorScroll();
      $location.hash();
    };

    // Change From => To in To => From
    $scope.changeLocation = function () {
      var aux = angular.copy($scope.temp.to);
      $scope.temp.to.name = $scope.temp.from.name;
      $scope.temp.to.lat = $scope.temp.from.lat;
      $scope.temp.to.lng = $scope.temp.from.lng;

      $scope.temp.from.name = aux.name;
      $scope.temp.from.lat = aux.lat;
      $scope.temp.from.lng = aux.lng;
    };

    // When press Cancel button clear all fields from booking
    $scope.Clear = function () {
      document.getElementById("name").value = "";
      document.getElementById("email").value = "";
      document.getElementById("phone_number").value = "";
      document.getElementById("car_type").value = "";
      document.getElementById("payment").value = "";
      document.getElementById("date").value = "";
      document.getElementById("time").value = "";
      document.getElementById("from").value = "";
      document.getElementById("to").value = "";
    };

    // Calculate distance from one point to another
    function toRad(Value) {
      return Value * Math.PI / 180;
    }

    function calcCrow(lat1, lon1, lat2, lon2) {
      var R = 6371; // km
      var dLat = toRad(lat2 - lat1);
      var dLon = toRad(lon2 - lon1);
      var lat1 = toRad(lat1);
      var lat2 = toRad(lat2);

      var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
        Math.sin(dLon / 2) * Math.sin(dLon / 2) * Math.cos(lat1) * Math.cos(lat2);
      var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
      var d = R * c;
      return d;
    }

    $scope.calculeazaDistanta = function () {
      $scope.calculDistanta = calcCrow($scope.temp.from.lat, $scope.temp.from.lng, $scope.temp.to.lat, $scope.temp.to.lng).toFixed(1);
    };

    // After finish completing booking go to my profile to se it
    $scope.goToMyBookings = function () {
      $location.path("/myBookings");
    };


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // $scope.validateForm = function()
    // {
    //   var x = document.forms["email"].value;
    //   if (x == "") {
    //     alert("E-mail must be filled out!");
    //     return false;
    //   }
    //   var y = document.forms["phone_number"].value;
    //   if (y == "") {
    //     alert("Phone number must be filled out!");
    //     return false;
    //   }
    // };
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // //This function takes in latitude and longitude of two location and returns the distance between them as the crow flies (in km)
    // //
    // // Converts numeric degrees to radians
    // function toRad(Value)
    // {
    //   return Value * Math.PI / 180;
    // }
    // $scope.calculeazaDistanta = function ()
    // {
    //   $scope.calculDistanta = calcCrow($scope.temp.from.lat,$scope.temp.from.lng,$scope.temp.to.lat,$scope.temp.to.lng).toFixed(1);
    // };
    //
    // $scope.calculatePriceAccordingToKm = function()
    // {
    //   Number($scope.calculDistanta);
    //   var estimatedPrice = 0;
    //   if($scope.calculDistanta >= 60 && $scope.calculDistanta <= 150)
    //   {
    //     $scope.estimatedPrice = $scope.calculDistanta / 2;
    //   }else if($scope.calculDistanta > 150 && $scope.calculDistanta <= 300)
    //   {
    //     $scope.estimatedPrice = $scope.calculDistanta / 3;
    //   }else if($scope.calculDistanta > 300)
    //   {
    //     $scope.estimatedPrice = $scope.calculDistanta / 4;
    //   }else
    //   {
    //     $scope.estimatedPrice = $scope.calculDistanta;
    //   }
    // };
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // $scope.calculatePriceAccordingToCarType = function ()
    // {
    //   var masinaSelectata = JSON.parse($scope.masinaSelectata);
    //   let finalPrice = 0;
    //   if(masinaSelectata != '' )
    //   {
    //     Number($scope.calculDistanta);
    //     Number(masinaSelectata.car_price);
    //     if($scope.calculDistanta >= 60 && $scope.calculDistanta <= 150) {
    //       $scope.finalPrice = ($scope.calculDistanta + masinaSelectata.car_price) / 3;
    //     }else if($scope.calculDistanta > 150 && $scope.calculDistanta <= 300) {
    //       $scope.finalPrice = ($scope.calculDistanta + masinaSelectata.car_price) / 4;
    //     }else if($scope.calculDistanta > 300) {
    //       $scope.finalPrice = ($scope.calculDistanta + masinaSelectata.car_price) / 5;
    //     }else {
    //       $scope.finalPrice = ($scope.calculDistanta + masinaSelectata.car_price) / 2;
    //     }
    //   }
    // };
  });
