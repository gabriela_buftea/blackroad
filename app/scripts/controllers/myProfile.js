'use strict';

/**
 * @ngdoc function
 * @name proiectApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the proiectApp
 */
angular.module('proiectApp')
  .controller('MyProfileCtrl', function ($scope, $timeout, $location) {
    $scope.goToMyProfileSaved = function () {
      $timeout(function () {
        $location.path("/myProfileSaved");
      }, 1);
    };

    //PUT request for Edit Profile
    $scope.saveProfileRequest = function () {
      var data = {};

      if ($scope.user_type)
        data.user_type = $scope.user_type;
      if ($scope.role)
        data.role = $scope.role;
      if ($scope.type)
        data.type = $scope.type;
      if ($scope.address)
        data.address = $scope.address;
      if ($scope.phone_number)
        data.phone_number = $scope.phone_number;
      if ($scope.email)
        data.email = $scope.email;
      if ($scope.country)
        data.country = $scope.country;

      var $http = new XMLHttpRequest();
      $http.open('PUT', 'http://5.2.191.107:11111/api/user/' + sessionStorage.getItem('id'), true);
      //Send the proper header information along with the request
      $http.setRequestHeader('Content-type', 'application/json');
      $http.setRequestHeader('Authorization', 'Basic ' + btoa(unescape(encodeURIComponent('email@gmail.com' + ':' + 'parola'))));

      $http.onreadystatechange = function () {//Call a function when the state changes.
        if ($http.readyState == 4 && $http.status == 200) {
          // alert($http.responseText);
        }
      };
      $http.send(JSON.stringify(data));
      console.dir(data);
    }
  });
