'use strict';

angular.module('proiectApp')
  .controller('SigninCtrl', function ($scope, $timeout, $location) {
    $scope.goToMyProfileSaved = function () {
      var string = btoa($scope.temp.email);
      window.localStorage.setItem('LogIn2', string);
      $timeout(function () {
        $location.path("/myProfileSaved");
      }, 1);
    };

    $scope.goToLogin = function () {
      $location.path("/login");
    };

    $scope.checkTerms = function () {
      if (document.getElementById('terms_conditions').checked) {

      } else {
        alert("You must accept terms and conditions!");
      }
    };

    $scope.register = function () {
      var gender = '';
      if (document.getElementById('radios-female').checked)
        gender = document.getElementById('radios-female').value;
      if (document.getElementById('radios-male').checked)
        gender = document.getElementById('radios-male').value;
      if (document.getElementById('radios-unknown').checked)
        gender = document.getElementById('radios-unknown').value;

      var data = {
        id_company: "1",
        user_type: $scope.user_type,
        role: $scope.role,
        type: $scope.type,
        password: $scope.password,
        id_account: "1",
        name: $scope.name,
        address: $scope.address,
        email: $scope.temp.email,
        phone_number: $scope.phone_number,
        gender: gender
      };
      var $http = new XMLHttpRequest();
      $http.open('POST', 'http://5.2.191.107:11111/api/user', true);

      //Send the proper header information along with the request
      $http.setRequestHeader('Content-type', 'application/json');
      $http.setRequestHeader('Authorization', 'Basic ' + btoa(unescape(encodeURIComponent('email@gmail.com' + ':' + 'parola'))));

      $http.onreadystatechange = function () {//Call a function when the state changes.
        if ($http.readyState == 4 && $http.status == 200) {
          alert($http.responseText);
        }
      };
      $http.send(JSON.stringify(data));
    };
  });
