'use strict';

var app = angular.module('proiectApp').controller('MyBookingsCtrl', function ($scope, $http, $location) {
  $scope.bookings =
    [{
      id: '',
      name: '',
      email: '',
      phone_number: '',
      car_type: '',
      price: '',
      payment: '',
      date: '',
      time: '',
      from: '',
      to: '',
      actions: ''
    }
    ];
  $scope.editingData = {};
  $scope.deleteBooking = function () {
    $scope.bookings.shift();
  };

  $scope.confirmBooking = function () {
    $(document).ready(function () {
      $('tr').click(function () {
        //Check to see if background color is set or if it's set to white.
        if (this.style.background == "" || this.style.background == "white") {
          $(this).css('background', '#74D165');
        }
        else {
          $(this).css('background', 'white');
        }
      });
    });
  };

  $scope.modify = function (booking) {
    $scope.editingData[booking.id] = true;
  };

  $scope.update = function (booking) {
    $scope.editingData[booking.id] = false;
  };

  $scope.start = function () {
    $http.get('http://5.2.191.107:11111/api/booking/search/client/' + sessionStorage.getItem('id'), {
        // params:  {page: 1, limit: 100, sort: 'name', direction: 'desc'},
        headers: {'Authorization': 'Basic ' + btoa(unescape(encodeURIComponent('email@gmail.com' + ':' + 'parola')))}
      }
    )
      .then(function (response) {
        console.dir(response.data);
        $scope.data = angular.copy(response.data);

        var id_map = {};
        for (var i = 0; i < response.data.length; i++) {
          id_map[i + 1] = response.data[i]['id'];
        }

        sessionStorage.setItem('id_map', JSON.stringify(id_map));

        var length = $scope.data.length;
        for (var i = 0; i < length; i++) {
          $scope.editingData[$scope.data[i].id] = false;
        }

        // console.dir(JSON.parse(sessionStorage.getItem('id_map')));

      }, function (x) {
        console.dir(x);
      });
  };
  $scope.start();

  $scope.editBookingReq = function (booking) {
    $scope.booking = booking;
    $scope.data =
      {
        name: $scope.booking.name,
        email: $scope.booking.email,
        phone_number: $scope.booking.phone_number,
        cartype: $scope.booking.car_type,
        price: $scope.booking.price,
        payment_method: $scope.booking.payment,
        date: $scope.booking.date,
        time: $scope.booking.time,
        from_address: $scope.booking.from,
        to_address: $scope.booking.to
      };

    var $http = new XMLHttpRequest();
    $http.open('PUT', 'http://5.2.191.107:11111/api/booking/' + $scope.booking.id, true);

    //Send the proper header information along with the request
    $http.setRequestHeader('Content-type', 'application/json');
    $http.setRequestHeader('Authorization', 'Basic ' + btoa(unescape(encodeURIComponent('email@gmail.com' + ':' + 'parola'))));

    $http.onreadystatechange = function () {//Call a function when the state changes.
      if ($http.readyState == 4 && $http.status == 200) {
      }
    };
    $http.send(JSON.stringify($scope.data));
  };

  $scope.deleteBookingReq = function (booking) {
    $scope.booking = booking;
    var $http = new XMLHttpRequest();
    $http.open('DELETE', 'http://5.2.191.107:11111/api/booking/' + $scope.booking.id, true);

    //Send the proper header information along with the request
    $http.setRequestHeader('Content-type', 'application/json');
    $http.setRequestHeader('Authorization', 'Basic ' + btoa(unescape(encodeURIComponent('email@gmail.com' + ':' + 'parola'))));

    $http.onreadystatechange = function () {//Call a function when the state changes.
      if ($http.readyState == 4 && $http.status == 200) {
        location.reload();
      }
    };
    $http.send();
  }

});
