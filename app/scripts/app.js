'use strict';

/**
 * @ngdoc overview
 * @name proiectApp
 * @description
 * # proiectApp
 *
 * Main module of the application.
 */
var app = angular
  .module('proiectApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ]);

  app.config(function ($routeProvider, $locationProvider)
  {
    $locationProvider.hashPrefix('');
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .when('/myProfile', {
        templateUrl: 'views/myProfile.html',
        controller: 'MyProfileCtrl',
      })
      .when('/myProfileSaved', {
        templateUrl: 'views/myProfileSaved.html',
        controller: 'MyProfileSavedCtrl',
      })
      .when('/login', {
        templateUrl: 'views/login.html',
        controller: 'LoginCtrl',
      })
      .when('/signin', {
        templateUrl: 'views/signin.html',
        controller: 'SigninCtrl',
      })
      .when('/myBookings', {
        templateUrl: 'views/myBookings.html',
        controller: 'MyBookingsCtrl',
      })
      .otherwise({
        redirectTo: '/'
      });
});

  let global = {
    isLoggedIn: false
  };

app.run(['$rootScope', '$location','globalSettings', function ($rootScope, $location, globalSettings)
{
  $rootScope.$on('$locationChangeStart', function(event)
  {
    var publicPage = ['/signin', '/login'];
    if(window.localStorage.getItem('LogIn2'))
    {
      globalSettings.isLoggedIn = true;
    }
    else
    {
      global.isLoggedIn = false;
      globalSettings.isLoggedIn = false;
      if(publicPage.indexOf($location.$$url) == -1)
        $location.path('/login');
      else $location.path($location.$$url);
    }

  });
}]);
