app.directive('menu', function($location, $timeout, globalSettings) {
  return {
    restrict: 'E',
    templateUrl: '../../views/appMenu.html',
    replace: true,
    controller: function ($scope)
    {
      $scope.variabila = globalSettings;

      $scope.logOut = function () {
        window.localStorage.clear();
        $location.path('/login');
        sessionStorage.setItem("email", null);
        sessionStorage.setItem("password", null);
        sessionStorage.setItem("id", null);
        sessionStorage.setItem("logged", 'false');
      };

      $scope.goToMyProfileSaved = function ()
      {
        $timeout(function()
        {
          $location.path("/myProfileSaved");
        },1);
      };

      $scope.goToMyBookings = function ()
      {
        $location.path("/myBookings");
      };
    }
  }
});
